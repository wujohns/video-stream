/**
 * 参考资料
 * https://tech.meituan.com/2016/07/08/stream-basics.html
 * https://www.mtyun.com/library/stream-internals
 * https://tech.meituan.com/2016/07/22/stream-in-action.html
 */

const fs = require('fs')
const ffmpeg = require('fluent-ffmpeg')
const path = require('path')
const stream = require('stream')

// 在 frame 目录下的文件
const dir = path.join(__dirname, './frame')
const files = fs.readdirSync(dir)

class FrameStream extends stream.Readable {
  constructor () {
    super()
    this.index = 1    // 避免受到 .DS_Store 的干扰，实际操作中从 0000000000.xxx 文件开始
    this.fileBuffer = null
    this.fileIndex = 0
  }

  _read (n) {
    if (this.index < files.length) {
      if (!this.fileBuffer) {
        const fileName = path.join(dir, files[this.index])
        this.fileBuffer = fs.readFileSync(fileName)
      }

      let end = n + this.fileIndex
      if (end > this.fileBuffer.length) {
        end = this.fileBuffer.length
        this.push(this.fileBuffer.slice(this.fileIndex, end))
        this.fileIndex = 0
        this.index++
        this.fileBuffer = null
      } else {
        this.push(this.fileBuffer.slice(this.fileIndex, end))
        this.fileIndex = end
      }
    } else {
      this.push(null)
      return
    }
  }
}
const frameStream = new FrameStream()

/**
 * 这里做了一个验证：
 * 1. fluent-ffmpeg 是可以接受由多个文件拼接起来的 stream 作为其 input 参数
 * 2. 依据 stream 原理以及上述验证，在 ffcreator 是可以支持 stream 的方式来生成视频的
 */
const command = ffmpeg()
command.addInput(frameStream)
command.inputOptions([
  '-framerate',
  60,
  '-video_size',
  '1920x1080'
])
command.output(path.join(__dirname, './kk.mp4'))
command.outputOptions([
  '-movflags',
  'faststart',
  '-pix_fmt',
  'yuv420p',
  '-r',
  24,
])

command.on('error', (err, stdout, stderr) => {
  console.log(err)
})

command.run()
